###########################################################################################
#### The code in this if block runs once per abi. Sets flags and prepares android libs ####
###########################################################################################
defs = abi_defs[host_machine.cpu()]

# Prepare dirs to look for android libraries #
lib_dirs = [
    toolchain / 'sysroot' / 'usr' / 'lib' / defs['cpu_family'] + '-linux-' + defs['eabi'],
    toolchain / 'sysroot' / 'usr' / 'lib' / defs['cpu_family'] + '-linux-' + defs['eabi'] / android_target_ver,
    android_ndk / 'platforms' / 'android-' + android_target_ver / 'arch-' + defs['cpu_family'] / 'usr' / 'lib'
]

# Find libandroid.so and liblog.so #
cc = meson.get_compiler('c')
android_lib = cc.find_library('android', dirs : lib_dirs)
log_lib = cc.find_library('log', dirs : lib_dirs)

# Compile the android native app glue code to a static library #
app_glue_inc = include_directories(android_ndk / 'sources' / 'android' / 'native_app_glue')
native_app_glue_lib = static_library('native_app_glue',
    android_ndk / 'sources' / 'android' / 'native_app_glue' / 'android_native_app_glue.c',
    include_directories: app_glue_inc, install: false)

# Make all libraries available as dependency #
android_app_dep = declare_dependency(
    link_with: [native_app_glue_lib],
    link_args: ['-u ANativeActivity_onCreate'],
    dependencies: [android_lib, log_lib],
    include_directories: app_glue_inc)

# You can use this variable to add the abi to your library name. E.g. libexample_x86.so
abi_suffix = '_' + defs['android_abi']

# Compile an example in case this project is not used as subproject #
if not meson.is_subproject()
    shared_library('dummy' + abi_suffix, 'example_dummy.cpp', dependencies: [android_app_dep], install: true)
endif
